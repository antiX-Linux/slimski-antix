# Install script for directory: /home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "None")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/slimski.theme")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/slimski.theme")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/panel.png")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/panel.png")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/background.jpg")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/background.jpg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/ar_EG;/usr/share/slimski/themes/default/bg_BG;/usr/share/slimski/themes/default/ca_ES;/usr/share/slimski/themes/default/cs_CZ;/usr/share/slimski/themes/default/da_DK;/usr/share/slimski/themes/default/de_AT;/usr/share/slimski/themes/default/de_CH")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/ar_EG"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/bg_BG"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/ca_ES"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/cs_CZ"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/da_DK"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/de_AT"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/de_CH"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/de_DE;/usr/share/slimski/themes/default/et_EE;/usr/share/slimski/themes/default/en_AU;/usr/share/slimski/themes/default/en_GB;/usr/share/slimski/themes/default/en_NZ;/usr/share/slimski/themes/default/en_US;/usr/share/slimski/themes/default/es_ES")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/de_DE"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/et_EE"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/en_AU"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/en_GB"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/en_NZ"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/en_US"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_ES"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/es_AR;/usr/share/slimski/themes/default/es_BO;/usr/share/slimski/themes/default/es_CO;/usr/share/slimski/themes/default/es_EC;/usr/share/slimski/themes/default/es_MX;/usr/share/slimski/themes/default/es_NI;/usr/share/slimski/themes/default/es_PE")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_AR"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_BO"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_CO"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_EC"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_MX"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_NI"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_PE"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/es_US;/usr/share/slimski/themes/default/es_UY;/usr/share/slimski/themes/default/es_VE;/usr/share/slimski/themes/default/eu_ES;/usr/share/slimski/themes/default/fa_IR;/usr/share/slimski/themes/default/fr_FR;/usr/share/slimski/themes/default/fr_BE")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_US"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_UY"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/es_VE"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/eu_ES"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/fa_IR"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/fr_FR"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/fr_BE"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/fr_CA;/usr/share/slimski/themes/default/fr_CH;/usr/share/slimski/themes/default/ka_GE;/usr/share/slimski/themes/default/el_GR;/usr/share/slimski/themes/default/hr_HR;/usr/share/slimski/themes/default/is_IS;/usr/share/slimski/themes/default/it_IT")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/fr_CA"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/fr_CH"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/ka_GE"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/el_GR"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/hr_HR"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/is_IS"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/it_IT"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/he_IL;/usr/share/slimski/themes/default/ja_JP;/usr/share/slimski/themes/default/kk_KZ;/usr/share/slimski/themes/default/ko_KR;/usr/share/slimski/themes/default/lv_LV;/usr/share/slimski/themes/default/lt_LT;/usr/share/slimski/themes/default/hu_HU")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/he_IL"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/ja_JP"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/kk_KZ"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/ko_KR"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/lv_LV"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/lt_LT"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/hu_HU"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/mk_MK;/usr/share/slimski/themes/default/nl_NL;/usr/share/slimski/themes/default/nl_BE;/usr/share/slimski/themes/default/nb_NO;/usr/share/slimski/themes/default/pl_PL;/usr/share/slimski/themes/default/pt_PT;/usr/share/slimski/themes/default/pt_BR")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/mk_MK"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/nl_NL"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/nl_BE"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/nb_NO"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/pl_PL"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/pt_PT"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/pt_BR"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/ro_RO;/usr/share/slimski/themes/default/ru_RU;/usr/share/slimski/themes/default/sq_AL;/usr/share/slimski/themes/default/zh_CN;/usr/share/slimski/themes/default/sk_SK;/usr/share/slimski/themes/default/sl_SI;/usr/share/slimski/themes/default/sr_RS")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/ro_RO"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/ru_RU"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/sq_AL"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/zh_CN"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/sk_SK"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/sl_SI"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/sr_RS"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/slimski/themes/default/fi_FI;/usr/share/slimski/themes/default/sv_SE;/usr/share/slimski/themes/default/zh_TW;/usr/share/slimski/themes/default/tr_TR;/usr/share/slimski/themes/default/uk_UA")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/share/slimski/themes/default" TYPE FILE FILES
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/fi_FI"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/sv_SE"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/zh_TW"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/tr_TR"
    "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/themes/default/uk_UA"
    )
endif()


# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/PAM.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/PAM.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/app.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/app.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/capslock.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/capslock.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/cfg.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/cfg.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/image.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/image.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/log.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/log.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/main.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/main.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/numlock.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/numlock.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/panel.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/panel.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/switchuser.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/switchuser.cpp.o"
  "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/util.cpp" "/home/anticap/Downloads/antiX-repos/slimski-1.5.0/obj-x86_64-linux-gnu/CMakeFiles/slimski.dir/util.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "PACKAGE=\"slimski\""
  "PKGDATADIR=\"/usr/share/slimski\""
  "SYSCONFDIR=\"/etc\""
  "VERSION=\"1.5.0\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "/usr/include/freetype2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

set (DOC "/usr/share/doc/slimski")

###   https://cmake.org/cmake/help/v3.0/command/install.html?highlight=install%20files
install(FILES  ChangeLog_slimski.txt  COPYING  slimski_OPTIONS_LIST.txt  DESTINATION ${DOC})
install(FILES  pam.sample THEMES  xinitrc.sample  DESTINATION ${DOC})
install(FILES  translations_TEMPLATE.txt  slimski_man.txt  A-Z__theming_optionslist.txt  DESTINATION ${DOC})

        (stuffing this here, for now)
        pedantic secuity disclaimer:
        The slimski "lastused lookup" convenience feature
        arguably presents an "information disclosure" vulnerability,
        enabling a person to probe "Does a login account 'bob' exist
        on this system? Does a 'barbara' login account exist? ..."
        -=-
        Any sysadmin concerned about and wishing to avoid such
        potential "probing" can:
        1) (on a fresh system) place a 0byte, read-only
           /etc/skel/.config/slimski.lastused
           or
        2) edit, self-compile, and install a modified slimski
           after editing panel.cpp::readLastUsed
           and inserting a line stating "return true;" (noquotes)
           as the first line within that function in order to
           suppress the "lastused lookup" feature.






::: list of available slimski configuration options
:::      ( This comprehensive list was gleaned from cfg.cpp )
::: A comprehensive list is not presented within the default-provided slimski.conf, nor within the manpage.



______________________________________________
INTRO

slimski provides approx 100 configurable variables.

At startup, slimski first loads a set of default (implicit) variable|value pairs.
A non-blank value is assigned to each variable, except for the
following (these DO receive a blank/empty initial value):

    default_user, F5_cmd, F11_cmd, sessionstart_cmd,
    sessionstop_cmd, xserver_arguments

Next, slimski reads the contents of /etc/slimski.conf
and any var|value lines therein will override any implicitly-set values.

Next, slimski checks whether a localized theme variant exists in the
directory of the specified theme (e.g. slimski__en_GB.theme)
It such exists,  slimski reads its contents and any var|value lines
therein will override any previously-set values.
-=-
If a localized variant (matching the user's runtime LANG env var) does not exist,
slimski reads the contents of the specified theme, and any var|value lines
therein will override any previously-set values.
-=-
(Yes, each theme (or localized variant) can specify differing, customized, options
thereby overriding any would-be global declarations found in slimski.conf)

______________________________________________

A few (listed below) of the slimski configuration options
are treated as boolean variables, which accept (expect) true or false
as their specified value. Notice that these are identifiable
by the _enabled suffix incorporated into their names.

    numlock_enabled, daemonmode_enabled, exit_enabled,
    passwdfocus_enabled, autologin_enabled, mouse_enabled,
    systemhalt_enabled, reboot_enabled
______________________________________________



::: Note:
:::    welcome_msg accepts two (optional) variables: %host and %domain
:::    atlogin_cmd accepts two (optional) variables: %sessiontype and %theme
:::
:::    sessionstart_cmd (executed right before atlogin_cmd) and sessionstop_cmd
:::    both accept %username as a variable parameter to be passed
welcome_msg    welcome_msg specified in en_US theme
welcome_x               10%
welcome_y               10%
welcome_font            Verdana:size=11:bold:slant=italic
welcome_color           #d7dde8



::: F1 message. Displayed withing the bounds of the panel.
:::
::: The as-shipped default reflects the fact that antiX preinstalls
::: several window managers and provides multiple sessiontypes.
::: If, on your system, you are only offering ONE sessiontype, you
::: would -1 (hide) this message or repurpose the msg textstring.
:::
F1_font            Verdana:size=12:bold
F1_color              #666666
F1_x                  10%
F1_y                  90%
F1_msg             (Press F1 to change sessiontype)

::: atlogin_cmd is the commandstring to be executed after a succesful login.
::: You can, optionally, instruct slimski to pass the %sessiontype and
::: %theme variable namestrings as commandline options.
::: This enables you to accommodate conditional launch of commands in .xinitrc
::: (conditional, based on the passed-in themename and/or sessiontype strings)
:::
::: If the %theme variable is passed along (via atlogin_cmd, configured within slimski.conf)
::: ...when a localized theme variant is found, and used, the theme string passed
::: will be the theme basename ~~ an indicative-of-locale suffix is not passed.


default_path       /usr/local/bin:/usr/bin
default_xserver    /usr/bin/X11/X

:::  THE VALUE FOR THIS OPTION MAY BE LEFT BLANK
xserver_arguments  -nolisten tcp

::::::::: For a list of available server_arguments, refer to
::::::::: http://www.x.org/archive/X11R6.8.1/doc/Xserver.1.html



::: Full path to the xauth binary
xauth_path         /usr/bin/xauth
#                     ^------    executable "xauth" does exist in two places.  antiX19 "which xauth" reports "/usr/bin/xauth"

numlock_enabled       false
daemonmode_enabled    true
atlogin_cmd           exec /usr/bin/bash -login ~/.xinitrc %sessiontype
systemhalt_cmd        /usr/sbin/shutdown -h now
reboot_cmd            /usr/sbin/shutdown -r now


:::  THE VALUE FOR THIS OPTION MAY BE LEFT BLANK
::: (if specified, will be executed immediately prior to atlogin_cmd)
::: (its implicit value is blank/empty if not specified, or outcommented)
sessionstart_cmd

:::  THE VALUE FOR THIS OPTION MAY BE LEFT BLANK
::: (if specified, will be executed immediately prior to atlogin_cmd)
::: (its implicit value is blank/empty if not specified, or outcommented)
::: Example usage: invoke a script to unmount NFS mounts, samba shares
sessionstop_cmd


::: the default  suspend_cmd  is blank.  You must investigate
::: what commandstring is appropriate for your operating system.
::: No enable|disable toggle provided (conf can leave blank to disable)
#suspend_cmd     /usr/sbin/suspend
#suspend_cmd     /usr/sbin/pm-suspend
#suspend_cmd     /usr/sbin/pm-suspend-hybrid


::: if popfirst_cmd is non-blank, and slimski is started with -k commandline option
::: (and autologin is set false)
::: slimski will launch the specified command//program immediately after painting the
::: login panel. If the command launches a windowed program, it window will appear
::: in front of (aka stacked atop) the panel. "Your mileage may vary" ~~ test to
::: determine which programs are (or are not) well-suited. The expectation is
::: that a windowed text reader might be launched to display helptext, tips,
::: terms-n-conditions or whatever you might care to, unmistakeably, call to
::: the users' attention.
:::
::: Bear in mind that no window manager is running. A fullscreen program will not
::: have a window "titlebar". Ensure the user is able to, and understands how to
::: close the popped up progra
popfirst_cmd       yad --color


::: CUSTOM COMMAND to be executed when pressing F11
#F11_cmd      import -window root /slimski.png
:::    ^--- for example, if IMAGEMAGICK IS INSTALLED
#  (requires scrot, which is preinstalled in antiX)
F11_cmd      scrot /root/slimski.png
#
::: Security note regarding custom commands:
:::     When choosing a command (or none), bear in mind that this command
:::     will be executed with elevated priviledges.
:::     For instance, it would be unwise (insecure) to run a command which
:::     writes an output file to /tmp/. Anyone with local access to the system
:::     could place a symlink there (at the known destination), a symlink pointing
:::     to a critical system file such as /usr/sbin/init

:::  F5_cmd  provides ANOTHER optional customizable command,
:::  will be executed if user presses the F5 key while viewing the slimski screen.
:::  If you decide to use this, carefully test the result of your called commandstring.
:::  It may be necessary for you to backslash//escape certain characters within
:::  the commandstring and/or necessary to enclose arguments within the commandstring
:::  using quotation marks.
:::
:::  NOTE: slimski performs no validation/checking to verify that the commandstring
:::        specified by you contains a valid (installed,callable) command.
:::        Also, slimski does not know/care which default shell interpreter
:::        will be receiving your commandstring. If your script/command
:::        depends on bash, you should ensure bash will be the handler, ala
:::        (((((       bash -c "mycommand"      )))))
:::
::: An innocuous command, useful for testing purposes:   yad --color
#F5_cmd

::: If, on your system(s), you do not wish to advertise the availability
::: of an individual Fkey -accessible command, you can place within your
::: conf file (or theme) a non-commented line declaring a blank value
::: for its associated messagestring.
#F11_msg
#F5_msg


::: In addition to F1, F5, F11...
::: You can configure keybinds for any of the remaining Fkeys.
:::
::: Leave blank / undefined any that you do not wish to assign.
:::
:::   Along with any configured cust 1--5 messages,
:::   the F2--f12 msgs are displayed to (emblazoned on) the background and
:::   their x,y offsets are relative to top-left corner of the overall screen.
#F2_cmd
#F2_msg
#F2_font
#f2_color
#F2_x
#F2_y

:::
:::    nothing prevents the user from repeatedly (accidentally?) pressing a given
:::    a given Fkey and thereby invoking multiple instances of an associated command/program
:::

:::
:::   F{2--12} nothing obliges you to assign a _cmd command
:::
:::   You can freely use the extra message slots to display
:::     any text(s), each at your chosen x,y coortinates and color




welcome_msg        Welcome to %host
sessiontype_msg        Sessiontype:

::: Immediately focus the password field.
::: This option is moot (ignored) unless a default_user has been specified
::: AND  singleInputMode is set false  AND autologin_enabled is set false
passwdfocus_enabled     true


::: Automatically login the default user (without entering the password)
::: If enabled, but no default_user has been specified ~~ no action.
:::        note: during "preview" mode, autologin is ignored/disabled
autologin_enabled       false

:::  When autologin_enabled is NOT set TRUE, the default_user option
:::  can be outcommented or left blank.
default_user


current_theme      default
authfile           /var/run/slimski.auth

systemhalt_msg     The system is halting...
reboot_msg         The system is rebooting...


:::   msg_font    and      msg_color
:::   these same option values are shared, (re)used, when displaying:
:::
:::        disabledviaconf_msg
:::        reboot_msg
:::        systemhalt_msg
:::        login_cmd_failed_msg  [only displayed in the event of a misconfiguration]
:::        teststring [hardcoded, displayed during testing, upon successful 'login']
:::
:::    Additionally, the msg_x  and  msg_y  options are applied to thes shared items.
:::    Take into consideration the length of the longest of these message strings when
:::    choosing the shared x,y coordinate values.
:::    (recommended: low x value, to achieve placement near left edge of panel)


::: THE VALUE FOR sessiontypes SHOULD BE COMMA-SEPARATED, NO SPACES
sessiontypes           fluxbox,openbox,icewm,startxfce4


::: When slimski launches, if the "default_user" conf variable is non-blank
::: the last-used sessiontype for that user can be prefilled. However,
::: if a default_user has not been specified, slimski will fallback to
::: using one of the configured sessiontypes... but which one?
::: Via the default_sessiontype variable, you instruct which one will
::: serve as the default.
default_sessiontype    fluxbox


::: Below, you may specify a mix of XFT or FREETYPE font names


:::     Theme-related options
input_panel_x      50%
input_panel_y      40%

input_name_x       200   //  for these 4 options, use only integer values (not percent)
input_name_y       154
input_pass_x       -1    // this default value yields a single inputbox
input_pass_y       -1

input_font         Verdana:size=11
input_color        #000000




:::Hide the input field textcursor (Valid values: true|false)
input_hidetextcursor       false
input_cursor_height    20
input_center_text      false
input_maxlength_name   20
input_maxlength_passwd 20


:::  If enabled, slimski will accept "exit" (noquotes) in the username
:::  input field as a command and will terminate without requesting a password,
:::  returning to console//tty prompt. Enabling this presents a security
:::  issue because, on current debian-derived operating systems, the
:::  slimski login manager program ncecessarily runs with elevated (root) permissions.
:::  (debian policy enforces: only root may launch the X server)
#exit_enabled    true


::: As with exit, each of the actions associated with a "special username"
::: may be independently specified as enabled or disabled
reboot_enabled          false
systemhalt_enabled      false
disabledviaconf_msg     disabled, per the current configuration


::: slimski will hide the mouse cursor while the login screen is displayed
::: unless TRUE is explicitly declared by a line here.
::: Consider: Displaying the cursor invites user confusion b/c the login page
::: contains no clickable elements. On the other hand, in the absence of
::: a cursor some users may jump to the conclusion "oh no, my mouse isn't working".
::: (note: while previewing themes, some window managers ignore this directive)
::: Valid values: true|false
# mouse_enabled          true


welcome_font           Verdana:size=14
welcome_color          #FFFFFF
welcome_x              -1
welcome_y              -1


background_style      stretch
background_color      #CCCCCC
 
username_font         Verdana:size=12
username_color        #FFFFFF
username_x            -1
username_y            -1
username_msg          Please enter your username

 
password_x      -1
password_y      -1
password_msg    Please enter your password
password_feedback_capslock_msg      Authentication failed (CapsLock is ON)
password_feedback_msg               Authentication failed

login_cmd_failed_msg       Failed to execute login command

::: these (msg_color, msg_font, msg_x, msg_y) are shared by, are applicable to:
:::       password_msg
:::       password_feedback_capslock_msg
:::       password_feedback_msg
msg_color     #FFFFFF
msg_font      Verdana:size=14:bold
msg_x         40
msg_y         40
 
sessiontype_color         #FFFFFF
sessiontype_font          Verdana:size=16:bold
sessiontype_x             50%
sessiontype_y             70%

______________________________________________
______________________________________________
______________________________________________


CUSTOMIZABLE MESSAGE LINES (aka "custom messages")

    cust1_msg, cust2_msg, cust3_msg, cust4_msg, cust5_msg

Each of these options may be left blank, or populated (by you) with
UTF-8 text strings. Each non-blank custom message will be displayed to the
rootwindow (aka "background", vs within the bounds of the panel image)
~~ each at your specified x/y coordinates, in your chosen color, using
your specified fontface and fontsize.

Bear in mind that slimski lacks ability to "wordwrap" any long lines,
and that the rendered "length" of a given line of text will (does) vary
depending on the dimensions|resolution of the end-user's display.

For each custom message you wish to display, five configuration lines
are expected. (Tempted to state "are required" ~~ at time of drafting
this documentation, all permutations have not been tested. A "fallback"
color seems likely; it would be applied by xlib in the absence of a stated
colorstring. Similarly, xlib may have a default|fallback font.)

cust1_color     #9999EE
cust1_font      Verdana:size=12
cust1_msg       cust1 _ msg      ¥·£·€·$·¢·₡·₢·₣·₤·₥·₦·₧·₨·₩·₪·₫·₭·₮·₯·₹
cust1_x         10%
cust1_y         10%

By the time you read this documentation, additional customizable message
option items may be available. The output shown by
   "slimski -s -p themename"
will serve as a definitive resource, in case this documentation lags

set (THEMES "themes/flat")

install(FILES slimski.theme DESTINATION ${PKGDATADIR}/${THEMES})
install(FILES panel.png DESTINATION ${PKGDATADIR}/${THEMES})
install(FILES background.png DESTINATION ${PKGDATADIR}/${THEMES})
install(FILES background_.png DESTINATION ${PKGDATADIR}/${THEMES})
install(FILES logo.png DESTINATION ${PKGDATADIR}/${THEMES})

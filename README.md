    slimski is a graphical login manager (aka DisplayManager) for X11.
    It aims to be simple, fast, and independent from the various desktop environments.

    Features:
    - supports customizable themes and provides a range of configuration options
    - panel provides PNG support (with alpha transparency)
    - remembers last sessiontype used; user can press F1 to cycle thru available xsessions
    - option to specify default_user and autologin
    - PNG / JPEG support for background images
    - supports use of freetype and XFT fonts
    - configurable inputbox presentation: dual, or single (GDM-style)
    - CMake build procedure
    - (same as SLiM) by design, slimski does NOT support XDMCP
    - Support non-latin characters in theme settings (as of v1.36, debian SLiM does not)
    - supports auth via libpam (same as SLiM)

INSTALLATION
    for generic instructions (non-debfile installation),
    refer to the  doc/INSTALL  file within this source package


content of "README", a file expected by debian packaging,
  is identical to "README.md" which is expected by git{hub,lab}
